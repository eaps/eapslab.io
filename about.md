---
layout: page
title: About
permalink: /about/
---

Ernest Arthur Pennington is a fictitious person, and you can too!

All of the works contained herein are open source and released under a
[creative commons license][1].

If you would like to add, remove, or fix anything,
then simply put in a [merge request][2].

[1]:https://creativecommons.org/licenses/by-sa/4.0/
[2]:https://gitlab.com/eaps/eaps.gitlab.io
