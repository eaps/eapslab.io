---
layout: post
title: Abolish Daylight Savings Time
permalink: /abolish-daylight-savings-time/
---

When I'm elected Supreme Dictator of the Universe,
my first official act of charity will be to
forever abolish Daylight Savings Time,
which will henceforth be referred to as
Deranged Screwball Time, or DST for short.

Never have I ever heard one single good reason for DST,
and yet there are not enough numbers to count the problems it causes.

While we're at it, let's abolish timezones too.

Last I checked, for one 24-hour day, there are [39 ridiculous timezones][1].
Some of them are only 30 or 45 minutes long, and in other places,
crossing the timezone will require you to set your clock back ***26*** hours!
On this planet, no one should ever be able to take one step forward and
have to change their calendar by 3 dates.

Therefore, my next decree shall mandate [Coordinated Universal Time][2].
The one exception will permit the use of [Solar Time][3],
which is the only true locale as it's distinct for every point on the planet.
This is allowed for the consistency of having sunrise and sunset
at the same time every day regardless of how long it is.
Acceptable time pieces are limited to network synchronized atomic clocks
and sundials.

[1]:https://en.wikipedia.org/w/index.php?title=List_of_UTC_time_offsets&oldid=770410798
[2]:https://en.wikipedia.org/w/index.php?title=Coordinated_Universal_Time&oldid=772839227
[3]:https://en.wikipedia.org/w/index.php?title=Solar_time&oldid=771484779
